import java.util.ArrayList;
import java.util.List;

public class Machine {

    public List<Integer> calculateScore(Move moveOfPlayer1, Move moveOfPlayer2) {
        //Define Rules

        // Calculate acc to rules

        //give score in arrya format
        List<Integer> score = new ArrayList<Integer>();
        if(moveOfPlayer1.equals(Move.COOPERATE) && moveOfPlayer2.equals(Move.COOPERATE)){
            score.add(2);
            score.add(2);
        } else if(moveOfPlayer1.equals(Move.CHEAT) && moveOfPlayer2.equals(Move.CHEAT)){
            score.add(0);
            score.add(0);
        } else if(moveOfPlayer1.equals(Move.CHEAT) && moveOfPlayer2.equals(Move.COOPERATE)){
            score.add(3);
            score.add(-1);
        }  else {
            score.add(-1);
            score.add(3);
        }
        return score;
    }
}
