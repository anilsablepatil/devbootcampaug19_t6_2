import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("-------------PLAYER1 --> COOPERATE /  PLAYER2 --> CHEAT---------------------");
        Behaviour cooperatingPlayer =() -> Move.COOPERATE;
        Behaviour cheatBehaviour =() -> Move.CHEAT;
        Machine machine = new Machine();

        Game game = new Game(new Player(cheatBehaviour), new Player(cooperatingPlayer),machine,2, System.out);
        game.play();

        System.out.println("--------------Enter Number of Rounds--------------------");
        System.out.println("------------------------------------------------------------");
        int rounds=scanner.nextInt();

        System.out.println("-------------PLAYER1 --> CONSOLE /  PLAYER2 --> CONSOLE---------------------");

        ConsoleBehaviour consolePlayer1 = new ConsoleBehaviour(new Scanner(System.in));
        ConsoleBehaviour consolePlayer2 = new ConsoleBehaviour(new Scanner(System.in));

        Game game2 = new Game(new Player(consolePlayer1), new Player(consolePlayer2),machine,2, System.out);
        game2.play();

    }
}
