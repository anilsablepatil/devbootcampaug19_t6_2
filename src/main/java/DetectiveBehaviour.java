import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class DetectiveBehaviour implements Behaviour, Observer {

    private List<Move> initialMoves;
    private boolean hasOpponentCheated = false;

    public DetectiveBehaviour(List<Move> initialMoves){
        this.initialMoves = initialMoves;
    }

    private Move myMove = Move.COOPERATE;

    public Move findOpponentsMove(Move[] moves) {
        return moves[0] == myMove ? moves[1] : moves[0];
    }

    public void setMyMove(Move move) {
        this.myMove = move;
    }

    @Override
    public Move move() {
        Move move = null;
        if(!initialMoves.isEmpty()){
            move = initialMoves.get(0);
            initialMoves.remove(0);
        }else{
            if(hasOpponentCheated) {
                return myMove;
            }
            return Move.CHEAT;
        }
        return move;
    }

    @Override
    public void update(Observable o, Object arg) {
        Move[] moves = (Move[]) arg;
        myMove = findOpponentsMove(moves);
        if(myMove == Move.CHEAT) {
            hasOpponentCheated = true;
        }
    }
}
