import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;


public class GameIntegrationTest {
    Game game;

    @Test
    public void shouldTestFirstRoundWithCoopAndCpyCat(){
        Player  player1 = new Player(new ConsoleBehaviour(new Scanner("COOPERATE")));
        Player player2 = new Player(new CopyCatBehaviourImpl());
        Machine machine = new Machine();
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:2 Score of second player: 2");

    }
    /*@Test
    public void shouldTestFirstRoundWithCheatAndCpyCat(){
        Player  player1 = new Player(new ConsoleBehaviour(new Scanner("CHEAT")));
        CopyCatBehaviourImpl behaviour = new CopyCatBehaviourImpl();
        Player player2 = new Player(behaviour);
        Machine machine = new Machine();
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(behaviour);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:0 Score of second player: 0");

    }*/
    @Test
    public void shouldTestFirstRoundWithCopyCatAsFirstPlayer(){
        Player  player2 = new Player(new ConsoleBehaviour(new Scanner("CHEAT")));
        Player player1 = new Player(new CopyCatBehaviourImpl());
        Machine machine = new Machine();
        int noOfRounds = 1;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:-1 Score of second player: 3");

    }

    @Test
    public void shouldTestFiveRoundWithCopyCatAsFirstPlayer(){
        Player  player2 = new Player(()-> Move.CHEAT);
        CopyCatBehaviourImpl behaviour = new CopyCatBehaviourImpl();
        Player player1 = new Player(behaviour);
        Machine machine = new Machine();
        int noOfRounds = 5;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(behaviour);
        //test play
        game.play();

        //verify the console output
        verify(console,times(5)).println("Score of first player:-1 Score of second player: 3");


    }

    @Test
    public void shouldTestFiveRoundWithCopyCatAsFirstPlayerAndSecondAlwaysCooperating(){
        Player  player2 = new Player(()-> Move.COOPERATE);
        Player player1 = new Player(new CopyCatBehaviourImpl());
        Machine machine = new Machine();
        int noOfRounds = 5;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:2 Score of second player: 2");
        verify(console).println("Score of first player:4 Score of second player: 4");
        verify(console).println("Score of first player:6 Score of second player: 6");
        verify(console).println("Score of first player:8 Score of second player: 8");
        verify(console).println("Score of first player:10 Score of second player: 10");


    }

    @Test
    public void shouldTestOneRoundWithAlwaysCooperateAndGrudge(){
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        Player player1 = new Player(grudgeBehaviour);
        Player  player2 = new Player(()-> Move.COOPERATE);
        Machine machine = new Machine();
        int noOfRounds = 2;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(grudgeBehaviour);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:2 Score of second player: 2");
        verify(console).println("Score of first player:4 Score of second player: 4");
    }

    @Test
    public void shouldTestOneRoundWithAlwaysCheatAndGrudge(){
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        Player player1 = new Player(grudgeBehaviour);
        Player  player2 = new Player(()-> Move.CHEAT);
        Machine machine = new Machine();
        int noOfRounds = 2;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(grudgeBehaviour);
        //test play
        game.play();

        //verify the console output
        verify(console, times(2)).println("Score of first player:-1 Score of second player: 3");
       // verify(console).println("Score of first player:-1 Score of second player: 3");
    }

    @Test
    public void shouldTestPasswithGrudgeAndCopyCatPlayer(){
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        Player player1 = new Player(grudgeBehaviour);

        CopyCatBehaviourImpl copyBehaviour = new CopyCatBehaviourImpl();
        Player  player2 = new Player(copyBehaviour);

        Machine machine = new Machine();
        int noOfRounds = 2;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(grudgeBehaviour);
        game.addObserver(copyBehaviour);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:2 Score of second player: 2");
        verify(console).println("Score of first player:4 Score of second player: 4");
    }

    @Test
    public void shouldTestPassWithGrudgeAndCopyCatPlayerWithFiveRound(){
        GrudgeBehaviour grudgeBehaviour = new GrudgeBehaviour();
        Player player1 = new Player(grudgeBehaviour);

        CopyCatBehaviourImpl copyBehaviour = new CopyCatBehaviourImpl();
        Player  player2 = new Player(copyBehaviour);

        Machine machine = new Machine();
        int noOfRounds = 5;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(grudgeBehaviour);
        game.addObserver(copyBehaviour);
        //test play
        game.play();

        //verify the console output
        verify(console).println("Score of first player:2 Score of second player: 2");
        verify(console).println("Score of first player:4 Score of second player: 4");
        verify(console).println("Score of first player:6 Score of second player: 6");
        verify(console).println("Score of first player:8 Score of second player: 8");
        verify(console).println("Score of first player:10 Score of second player: 10");
    }


    /* Testing Detective Behaviour */

    @Test
    public void shouldDetectiveBehaveAsAlwaysCheat() {

        Player player1 = new Player(()-> Move.COOPERATE);

        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(new ArrayList<Move> (Arrays.asList(Move.COOPERATE, Move.CHEAT, Move.COOPERATE, Move.COOPERATE)));
        Player player2 = new Player(detectiveBehaviour);

        Machine machine = new Machine();
        int noOfRounds = 6;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(detectiveBehaviour);

        //test play
        game.play();

        verify(console).println("Score of first player:2 Score of second player: 2");
        verify(console).println("Score of first player:1 Score of second player: 5");
        verify(console).println("Score of first player:3 Score of second player: 7");
        verify(console).println("Score of first player:5 Score of second player: 9");
        verify(console).println("Score of first player:4 Score of second player: 12");
        verify(console).println("Score of first player:3 Score of second player: 15");

    }

    @Test
    public void shouldDetectiveBehaveAsCopyCat() {

        Player player1 = new Player(()-> Move.CHEAT);

        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(new ArrayList<Move> (Arrays.asList(Move.COOPERATE, Move.CHEAT, Move.COOPERATE, Move.COOPERATE)));
        Player player2 = new Player(detectiveBehaviour);

        Machine machine = new Machine();
        int noOfRounds = 6;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(detectiveBehaviour);

        //test play
        game.play();

        verify(console, times(2)).println("Score of first player:3 Score of second player: -1");
        verify(console).println("Score of first player:6 Score of second player: -2");
        verify(console, times(3)).println("Score of first player:9 Score of second player: -3");

    }

    @Test
    public void shouldRunIntegrationTest() {
        CopyCatBehaviourImpl copyBehaviour = new CopyCatBehaviourImpl();
        Player player1 = new Player(copyBehaviour);

        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(new ArrayList<Move> (Arrays.asList(Move.COOPERATE, Move.CHEAT, Move.COOPERATE, Move.COOPERATE)));
        Player player2 = new Player(detectiveBehaviour);

        Machine machine = new Machine();
        int noOfRounds = 6;
        PrintStream console = mock(PrintStream.class);

        game = new Game(player1, player2, machine, noOfRounds, console);
        game.addObserver(detectiveBehaviour);
        game.addObserver(copyBehaviour);

        //test play
        game.play();

        verify(console, times(2)).println("Score of first player:3 Score of second player: -1");
        verify(console).println("Score of first player:6 Score of second player: -2");
        verify(console, times(3)).println("Score of first player:9 Score of second player: -3");

    }


}