import java.util.Observable;
import java.util.Observer;

public class GrudgeBehaviour implements Behaviour, Observer {

    private Move myMove = Move.COOPERATE;

    public Move findOpponentsMove(Move[] moves) {
        return moves[0] == myMove ? moves[1] : moves[0];
    }

    public void setMyMove(Move move) {
        this.myMove = move;
    }

    @Override
    public Move move() {
        return myMove;
    }


    @Override
    public void update(Observable o, Object arg) {
        if(myMove == Move.COOPERATE) {
            Move[] moves = (Move[]) arg;
            myMove = findOpponentsMove(moves);
        }
    }
}
