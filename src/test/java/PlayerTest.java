import org.junit.Assert;

import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {
    Player player;

    @Test
    public void shouldTestPlayerCooperate(){
        player = new Player(new ConsoleBehaviour(new Scanner(Move.COOPERATE.toString())));
        Assert.assertEquals(Move.COOPERATE, player.move());

    }

    @Test
    public void shouldTestPlayerCheat(){
        player = new Player(new ConsoleBehaviour(new Scanner("CHEAT")));
        Assert.assertEquals(Move.CHEAT, player.move());
    }

    @Test
    public void shouldGetAndSetScore() {
        player = new Player(new ConsoleBehaviour(new Scanner("")));
        player.setScore(10);
        Assert.assertEquals(10, player.getScore());
    }

    @Test
    public void shouldMaintainLastState(){
        player = new Player(()-> Move.COOPERATE);
        player.move();
        Assert.assertEquals(Move.COOPERATE, player.getLastState());
    }
}
