import java.util.Scanner;

// this is observable
public class Player {

    private int score;
    private Behaviour behaviour;
    private Move lastState;

    Player( Behaviour behaviour){
        this.behaviour = behaviour;

        this.score = 0;
    }

    public Move move() {
        lastState = behaviour.move();
        return lastState ;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score += score;
    }

    public Move getLastState() {
        return lastState;
    }
}