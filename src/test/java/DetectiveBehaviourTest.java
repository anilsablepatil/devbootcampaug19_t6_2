import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DetectiveBehaviourTest {

    @Test
    public void shouldCreateInstanceWithInitialMovesPopulated() {
        DetectiveBehaviour detectiveBehaviour = new DetectiveBehaviour(
                new ArrayList<Move> (Arrays.asList(Move.COOPERATE, Move.CHEAT, Move.COOPERATE, Move.COOPERATE)));
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        Assert.assertEquals(Move.CHEAT, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
        Assert.assertEquals(Move.COOPERATE, detectiveBehaviour.move());
    }

}
