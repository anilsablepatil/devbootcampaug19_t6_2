
import java.util.Observable;
import java.util.Observer;

// This is now Observer
public class CopyCatBehaviourImpl implements Behaviour, Observer {
    private Move myMove = Move.COOPERATE;

    @Override
    public Move move() {
        return myMove;
    }

    public void setMyMove(Move move) {
        this.myMove = move;
    }

    public Move findOpponentsMove(Move[] moves) {
        return moves[0] == myMove ? moves[1] : moves[0];
    }

    @Override
    public void update(Observable o, Object arg) {
        Move[] moves = (Move[]) arg;
        myMove = findOpponentsMove(moves);
    }
}
