import java.util.Scanner;

public class ConsoleBehaviour implements Behaviour{

    Scanner scanner;


    public ConsoleBehaviour(Scanner scanner) {
        this.scanner = scanner;
    }

    public Move move() {
        return  Move.valueOf(scanner.nextLine());
    }
}