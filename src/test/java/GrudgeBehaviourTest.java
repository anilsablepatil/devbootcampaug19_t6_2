import org.junit.Assert;
import org.junit.Test;

public class GrudgeBehaviourTest {

    @Test
    public void shouldPlayerCooperateWhenOtherPlayerCooperate() {
        GrudgeBehaviour behaviour = new GrudgeBehaviour();
        behaviour.setMyMove(Move.COOPERATE);
        Move opponentMove = behaviour.findOpponentsMove(new Move[] {Move.COOPERATE, Move.COOPERATE});
        Assert.assertEquals(Move.COOPERATE, opponentMove);
    }

    @Test
    public void shouldPlayerCheatWhenOtherPlayerCheat() {
        GrudgeBehaviour behaviour = new GrudgeBehaviour();
        behaviour.setMyMove(Move.COOPERATE);
        Move opponentMove = behaviour.findOpponentsMove(new Move[] {Move.CHEAT, Move.COOPERATE});
        Assert.assertEquals(Move.CHEAT, opponentMove);
    }
}
